package com.latihan.microservice.paymentservice.exception;

import com.latihan.microservice.paymentservice.dto.ErrorMessage;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestResponseErrorHandler {

    @ExceptionHandler(CustomException.class)
    public ResponseEntity<ErrorMessage> customHandler(CustomException customException){
        return new ResponseEntity<>(new ErrorMessage().builder()
                .message(customException.getMessage())
                .error(customException.getError())
                .build(),
                HttpStatusCode.valueOf(customException.getStatus()));
    }
}
