package com.latihan.microservice.paymentservice.service;

import com.latihan.microservice.paymentservice.dto.PaymentRequest;
import com.latihan.microservice.paymentservice.dto.PaymentResponse;

import java.util.List;

public interface PaymentService {

    public List<PaymentResponse> getAll();
    public PaymentResponse getById(Long id);
    public PaymentResponse create(PaymentRequest paymentRequest);

}
