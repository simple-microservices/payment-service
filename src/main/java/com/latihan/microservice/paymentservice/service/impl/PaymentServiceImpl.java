package com.latihan.microservice.paymentservice.service.impl;

import com.latihan.microservice.paymentservice.dto.PaymentRequest;
import com.latihan.microservice.paymentservice.dto.PaymentResponse;
import com.latihan.microservice.paymentservice.entity.Payment;
import com.latihan.microservice.paymentservice.exception.CustomException;
import com.latihan.microservice.paymentservice.repository.PaymentRepository;
import com.latihan.microservice.paymentservice.service.PaymentService;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PaymentServiceImpl implements PaymentService {

    private PaymentRepository paymentRepository;

    @Override
    public List<PaymentResponse> getAll() {
        return paymentRepository.findAll()
                .stream()
                .map(payment->{
                    PaymentResponse paymentResponse = new PaymentResponse();
                    BeanUtils.copyProperties(payment,paymentResponse);
                    return paymentResponse;
                }).collect(Collectors.toList());
    }

    @Override
    public PaymentResponse getById(Long id) {
        Payment payment = paymentRepository.findById(id)
                .orElseThrow(() -> new CustomException(
                        "Payment with id " + id + " not found",
                        "PAYMENT_NOT_FOUND",
                        404
                ));
        PaymentResponse paymentResponse = new PaymentResponse();
        BeanUtils.copyProperties(payment,paymentResponse);
        return paymentResponse;
    }

    @Override
    public PaymentResponse create(PaymentRequest paymentRequest) {
        Payment payment = Payment.builder()
                .orderId(paymentRequest.getOrderId())
                .amount(paymentRequest.getAmount())
                .mode(paymentRequest.getMode().name())
                .date(Instant.now())
                .status("SUCCESS")
                .build();
        paymentRepository.save(payment);

        PaymentResponse paymentResponse = new PaymentResponse();
        BeanUtils.copyProperties(payment,paymentResponse);
        return paymentResponse;
    }
}
