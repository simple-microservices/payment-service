package com.latihan.microservice.paymentservice.controller;

import com.latihan.microservice.paymentservice.dto.PaymentRequest;
import com.latihan.microservice.paymentservice.dto.PaymentResponse;
import com.latihan.microservice.paymentservice.dto.ResponseMessage;
import com.latihan.microservice.paymentservice.service.PaymentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/payments")
@AllArgsConstructor
public class PaymentController {

    private PaymentService paymentService;

    @GetMapping
    public ResponseEntity<ResponseMessage<List<PaymentResponse>>> getAll(){
        ResponseMessage response = new ResponseMessage<>();
        response.setData(paymentService.getAll());
        response.setMessage("List all payments.");
        response.setSuccess(true);
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseMessage<PaymentResponse>> getById(@PathVariable Long id){
        ResponseMessage response = new ResponseMessage<>();
        response.setData(paymentService.getById(id));
        response.setMessage("Detail data payment.");
        response.setSuccess(true);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ResponseMessage<PaymentResponse>> create(@RequestBody PaymentRequest paymentRequest){
        ResponseMessage response = new ResponseMessage<>();
        response.setData(paymentService.create(paymentRequest));
        response.setMessage("Success create payment.");
        response.setSuccess(true);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }


}
