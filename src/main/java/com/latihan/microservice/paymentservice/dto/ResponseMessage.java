package com.latihan.microservice.paymentservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseMessage<Y> {

    private Boolean success;

    private String message;

    private Y data;

}
