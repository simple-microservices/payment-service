package com.latihan.microservice.paymentservice.entity;

public enum PaymentMode {
    CASH,
    PAYPAL,
    DEBIT_CARD,
    CREDIT_CARD
}
